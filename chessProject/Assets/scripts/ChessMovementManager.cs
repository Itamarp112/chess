using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChessMovementManager : MonoBehaviour
{
    // Start is called before the first frame update
    string clientName = "";
    string source = "";
    string dest = "";
    public PawnMovement movement;
    public ClientCommunication communicationScript;
    public GameObject src;
    int count = 0;
    static bool turn;
    static bool isWhite;
    public GameObject camObject;
    public GameObject camPosition;
    public MeshCollider[] blackColliders;
    public MeshCollider[] whiteColliders;
    public GameObject blackPieces;
    public GameObject whitePieces;
    public ChessPawn[,] ChessBoard;
    public IDictionary<string, (int, int)> dict;


    void Start()
    {
        DictionarySet();
        ChessBoardStart();
        turn = communicationScript.GetUserColor();
        isWhite = communicationScript.GetUserColor();
        if (!isWhite)
        {
            SetAsBlack();
        }
    }

    // Update is called once per frame
    public void Source(string name)
    {
        source = name;
    }

    public void Dest(string name)
    {
        dest = name;
        if (turn && CheckLegalMove(source, dest))
        {
            //dest = name;
            clientName = communicationScript.GetUserName();
            communicationScript.SendInfrmaionToServer("MOVESEND;" + source + ";" + dest + ";" + clientName);
            string response = communicationScript.ReciveInformationFromServer();
            src = GameObject.Find(source);
            movement = src.GetComponent<PawnMovement>();
            movement.Move(dest);
            turn = false;
        }

    }

    void Update()
    {
        if (!turn)
        {
            if (count == 60)
            {
                clientName = communicationScript.GetUserName();
                communicationScript.SendInfrmaionToServer("MOVECHECK;" + clientName);
                string response = communicationScript.ReciveInformationFromServer();
                string[] responseSplit = response.Split(';');
                if (responseSplit[0].Equals("OpponentMoved"))
                {
                    source = responseSplit[1];
                    dest = responseSplit[2];
                    src = GameObject.Find(source);
                    movement = src.GetComponent<PawnMovement>();
                    movement.Move(dest);
                    turn = true;
                }
                count = 0;
            }
            else count++;
        }
    }

    void SetAsBlack()
    {
        camObject.transform.position = camPosition.transform.position;
        camObject.transform.rotation = camPosition.transform.rotation;
        blackColliders = blackPieces.GetComponentsInChildren<MeshCollider>();
        foreach (MeshCollider mesh in blackColliders)
        {
            mesh.enabled = true;
        }
        whiteColliders = whitePieces.GetComponentsInChildren<MeshCollider>();
        foreach (MeshCollider mesh in whiteColliders)
        {
            mesh.enabled = false;
        }

    }

    void ChessBoardStart()
    {
        ChessBoard = new ChessPawn[8, 8];
        Rock RockWhite2 = new Rock("white", "Rock White 2", 0, 0);
        Rock RockWhite = new Rock("white", "Rock White", 7, 0);
        Rock RockBlack = new Rock("black", "Rock Black", 0, 7);
        Rock RockBlack2 = new Rock("black", "Rock Black 2", 7, 7);
        ChessBoard[0, 0] = RockWhite2;
        ChessBoard[7, 0] = RockWhite;
        ChessBoard[0, 7] = RockBlack;
        ChessBoard[7, 7] = RockBlack2;
        Pawn PawnWhite = new Pawn("white", "Pawn White", 0, 1);
        Pawn PawnWhite2 = new Pawn("white", "Pawn White 2", 1, 1);
        Pawn PawnWhite3 = new Pawn("white", "Pawn White 3", 2, 1);
        Pawn PawnWhite4 = new Pawn("white", "Pawn White 4", 3, 1);
        Pawn PawnWhite5 = new Pawn("white", "Pawn White 5", 4, 1);
        Pawn PawnWhite6 = new Pawn("white", "Pawn White 6", 5, 1);
        Pawn PawnWhite7 = new Pawn("white", "Pawn White 7", 6, 1);
        Pawn PawnWhite8 = new Pawn("white", "Pawn White 8", 7, 1);
        ChessBoard[0, 1] = PawnWhite;
        ChessBoard[1, 1] = PawnWhite2;
        ChessBoard[2, 1] = PawnWhite3;
        ChessBoard[3, 1] = PawnWhite4;
        ChessBoard[4, 1] = PawnWhite5;
        ChessBoard[5, 1] = PawnWhite6;
        ChessBoard[6, 1] = PawnWhite7;
        ChessBoard[7, 1] = PawnWhite8;
        Pawn PawnBlack = new Pawn("Black", "Pawn Black", 0, 6);
        Pawn PawnBlack2 = new Pawn("Black", "Pawn Black 2", 1, 6);
        Pawn PawnBlack3 = new Pawn("Black", "Pawn Black 3", 2, 6);
        Pawn PawnBlack4 = new Pawn("Black", "Pawn Black 4", 3, 6);
        Pawn PawnBlack5 = new Pawn("Black", "Pawn Black 5", 4, 6);
        Pawn PawnBlack6 = new Pawn("Black", "Pawn Black 6", 5, 6);
        Pawn PawnBlack7 = new Pawn("Black", "Pawn Black 7", 6, 6);
        Pawn PawnBlack8 = new Pawn("Black", "Pawn Black 8", 7, 6);
        ChessBoard[0, 6] = PawnBlack;
        ChessBoard[1, 6] = PawnBlack2;
        ChessBoard[2, 6] = PawnBlack3;
        ChessBoard[3, 6] = PawnBlack4;
        ChessBoard[4, 6] = PawnBlack5;
        ChessBoard[5, 6] = PawnBlack6;
        ChessBoard[6, 6] = PawnBlack7;
        ChessBoard[7, 6] = PawnBlack8;
    }

    public bool CheckLegalMove(string source, string dest)
    {   //���� ������ ������ ������
        (int, int) location = dict[dest];
        int y = location.Item2;
        int x = location.Item1;
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (ChessBoard[i, j] != null && ChessBoard[i, j]._name.Equals(source))
                {
                    if (ChessBoard[i, j].IsLegalMove(x, y))
                    {
                        ChessBoard[x, y] = ChessBoard[i, j];
                        ChessBoard[x, y]._locationX = x;
                        ChessBoard[x, y]._locationY = y;
                        ChessBoard[i, j] = null;
                        return true;
                    }
                    return false;

                }
            }
        }
        return false;
    }

    void DictionarySet()
    {
        dict = new Dictionary<string, (int, int)>();
        dict.Add(("A1"), (0, 0));
        dict.Add(("A2"), (0, 1));
        dict.Add(("A3"), (0, 2));
        dict.Add(("A4"), (0, 3));
        dict.Add(("A5"), (0, 4));
        dict.Add(("A6"), (0, 5));
        dict.Add(("A7"), (0, 6));
        dict.Add(("A8"), (0, 7));
        dict.Add(("B1"), (1, 0));
        dict.Add(("B2"), (1, 1));
        dict.Add(("B3"), (1, 2));
        dict.Add(("B4"), (1, 3));
        dict.Add(("B5"), (1, 4));
        dict.Add(("B6"), (1, 5));
        dict.Add(("B7"), (1, 6));
        dict.Add(("B8"), (1, 7));
        dict.Add(("C1"), (2, 0));
        dict.Add(("C2"), (2, 1));
        dict.Add(("C3"), (2, 2));
        dict.Add(("C4"), (2, 3));
        dict.Add(("C5"), (2, 4));
        dict.Add(("C6"), (2, 5));
        dict.Add(("C7"), (2, 6));
        dict.Add(("C8"), (2, 7));
        dict.Add(("D1"), (3, 0));
        dict.Add(("D2"), (3, 1));
        dict.Add(("D3"), (3, 2));
        dict.Add(("D4"), (3, 3));
        dict.Add(("D5"), (3, 4));
        dict.Add(("D6"), (3, 5));
        dict.Add(("D7"), (3, 6));
        dict.Add(("D8"), (3, 7));
        dict.Add(("E1"), (4, 0));
        dict.Add(("E2"), (4, 1));
        dict.Add(("E3"), (4, 2));
        dict.Add(("E4"), (4, 3));
        dict.Add(("E5"), (4, 4));
        dict.Add(("E6"), (4, 5));
        dict.Add(("E7"), (4, 6));
        dict.Add(("E8"), (4, 7));
        dict.Add(("F1"), (5, 0));
        dict.Add(("F2"), (5, 1));
        dict.Add(("F3"), (5, 2));
        dict.Add(("F4"), (5, 3));
        dict.Add(("F5"), (5, 4));
        dict.Add(("F6"), (5, 5));
        dict.Add(("F7"), (5, 6));
        dict.Add(("F8"), (5, 7));
        dict.Add(("G1"), (6, 0));
        dict.Add(("G2"), (6, 1));
        dict.Add(("G3"), (6, 2));
        dict.Add(("G4"), (6, 3));
        dict.Add(("G5"), (6, 4));
        dict.Add(("G6"), (6, 5));
        dict.Add(("G7"), (6, 6));
        dict.Add(("G8"), (6, 7));
        dict.Add(("H1"), (7, 0));
        dict.Add(("H2"), (7, 1));
        dict.Add(("H3"), (7, 2));
        dict.Add(("H4"), (7, 3));
        dict.Add(("H5"), (7, 4));
        dict.Add(("H6"), (7, 5));
        dict.Add(("H7"), (7, 6));
        dict.Add(("H8"), (7, 7));
    }
}


public class ChessPawn
{
    public string _color { set; get; }
    public string _name { set; get; }
    public int _locationX { set; get; }
    public int _locationY { set; get; }


    public ChessPawn(string color, string name, int locationX, int locationY)
    {
        _color = color;
        _name = name;
        _locationY = locationY;
        _locationX = locationX;

    }
    public virtual bool IsLegalMove(int x, int y)
    {
        return false;
    }
}

public class Rock : ChessPawn
{
    public Rock(string _color, string _name, int _locationX, int _locationY) : base(_color, _name, _locationX, _locationY) { }

    public override bool IsLegalMove(int x, int y)
    {
        if (_locationX == x && _locationY != y)
        {
            for (int i = _locationY; i < y; i++)
            {
            }
            return true;

        }
        else if (_locationY == y && _locationX != x)
        {
            return true;

        }
        return false;
    }
}

public class Pawn : ChessPawn
{
    public Pawn(string _color, string _name, int _locationX, int _locationY) : base(_color, _name, _locationX, _locationY) { }

    public override bool IsLegalMove(int x, int y)
    {
        if ((_locationY == 1 && y == 3) || (y == _locationY + 1) && (_locationX == x))
        {
            return true;
        }
        return false;
    }
}
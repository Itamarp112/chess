﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace checkersBagrut_server
{
    class Game
    {
        public string _roomName { set; get; }
        public string _creatorName { set; get; }
        public string _memberName { set; get; }
        public Socket _creatorSocket { set; get; }
        public Socket _memberSocket { set; get; }
        public bool _opponentMoves { set; get; }
        public string _dest { set; get; }
        public string _source { set; get; }
        public string _senderName { set; get; }
        public bool _memberColor { set; get; } //true for white.
        public bool _creatorColor { set; get; } //true for white.
        public bool _gameOver { set; get; }

        public Game(Socket creatorSocket, Socket memberSocket, string roomName, string creatorName, string memberName, bool opponentMoves)
        {
            _roomName = roomName;
            _creatorSocket = creatorSocket;
            _memberSocket = memberSocket;
            _creatorName = creatorName;
            _memberName = memberName;
            _opponentMoves = false;
            _senderName =memberName;
            _creatorColor = false;
            _memberColor = false;
            _gameOver = false;
        }

        public Game()
        { }

        public void RoomCopy(Game other)
        {
            this._roomName = other._roomName;
            this._creatorSocket = other._creatorSocket;
            this._creatorName = other._creatorName;
            this._memberSocket = other._memberSocket;
            this._memberName = other._memberName;
        }
    }
}

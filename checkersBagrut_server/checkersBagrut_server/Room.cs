﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;

namespace checkersBagrut_server
{
    class Room
    {
        public string _roomName { set; get; }
        public string _creatorName { set; get; }
        public string _memberName { set; get; }
        public Socket _creatorSocket { set; get; }
        public Socket _memberSocket { set; get; }
        public bool _isLocked { set; get; }
        public bool _allowsViewer { set; get; }
        public bool _hasGameStarted { set; get; }

        public Room(Socket creatorSocket, string roomName, string creatorName, string memberName, bool isLocked, bool allowsViewer)
        {
            _roomName = roomName;
            _creatorSocket = creatorSocket;
            _creatorName = creatorName;
            _isLocked = isLocked;
            _memberName = memberName;
            _memberSocket = null;
            _allowsViewer = allowsViewer;
            _hasGameStarted = false;
        }

        public Room()
        {}

        public  void RoomCopy(Room other)
        {
            this._roomName = other._roomName;
            this._creatorSocket = other._creatorSocket;
            this._creatorName = other._creatorName;
        }

    }
}

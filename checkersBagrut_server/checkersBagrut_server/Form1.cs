﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Serialization;
namespace checkersBagrut_server
  {
    public partial class Form1 : Form
    {

        private static Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static byte[] Buffer = new byte[1024];


        private static List<Game> gameList = new List<Game>();
        private static List<loginUser> ClientLoginUsers = new List<loginUser>();
        private static List<userData> userList = new List<userData>();
        private static List<Room> roomList = new List<Room>();
        private static XmlSerializer xm;
        int threadCheck = 0;


        public Form1()
        {
            InitializeComponent();
            StartServer();       
        }


        private void StartServer()
        {
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, 6666));
            serverSocket.Listen(2);
            serverSocket.BeginAccept(new AsyncCallback(waitForNewConnection), serverSocket);
        }
        private void waitForNewConnection(IAsyncResult AR)
        {
            Socket socket = serverSocket.EndAccept(AR);
            socket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, new AsyncCallback(RecieveInformation), socket);
            serverSocket.BeginAccept(new AsyncCallback(waitForNewConnection), serverSocket);
        }
        private void RecieveInformation(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            IPEndPoint remoteIpEndPoint = socket.RemoteEndPoint as IPEndPoint;
            IPEndPoint localIpEndPoint = socket.LocalEndPoint as IPEndPoint;
            
            int recieve;

            try
            {
                recieve = socket.EndReceive(AR);
            }
            catch
            {
                return;
            }
            byte[] dataBuff = new byte[recieve];
            Array.Copy(Buffer, dataBuff, recieve);
            string text = Encoding.ASCII.GetString(dataBuff);
            string[] ClientInformation = text.Split(';');

            returnInformationToClient(socket, ClientInformation);
        }
        private void returnInformationToClient(Socket socket, string[] ClientInformation)
        {
            string result = "";
            xm = new XmlSerializer(typeof(List<userData>));

            if (ClientInformation[0].Equals("LOGIN"))
            {
                try
                {
                    FileStream fs1 = new FileStream(@"data\users.xml", FileMode.Open, FileAccess.Read);
                    userList = (List<userData>)xm.Deserialize(fs1);
                    fs1.Close();
                }
                catch
                {
                }
                if (!is_exist(userList, ClientInformation[1], ClientInformation[2], true))
                {
                    result = "ERROR: This user doesn't exists";
                }
                else if (is_logged_in(ClientLoginUsers, ClientInformation[1]) != -1)
                {
                    result = "ERROR: This user already logged in";
                }
                else
                {
                    result = "loged in seccesfully!!!";
                    ClientLoginUsers.Add(new loginUser(ClientInformation[1], socket));
                }
            }
            
            if (ClientInformation[0].Equals("REGISTER"))
            {
                try
                {
                    FileStream fs1 = new FileStream(@"data\users.xml", FileMode.Open, FileAccess.Read);
                    userList = (List<userData>)xm.Deserialize(fs1);
                    fs1.Close();

                    FileStream fs2 = new FileStream(@"data\users.xml", FileMode.Open, FileAccess.Write);
                    userData new_user = new userData(ClientInformation[1], ClientInformation[2]);

                    if (!legalName(ClientInformation[1]))
                    {
                        result = "ERROR: illegal name";
                    }
                    else if (is_exist(userList, ClientInformation[1], ClientInformation[2], false))
                    {
                        result = "ERROR: This user already exists";
                    }
                    else
                    {
                        result = "The user has registered successfully";
                        userList.Add(new_user);
                    }

                    xm.Serialize(fs2, userList);
                    fs2.Close();
                }
                catch (Exception e)
                {
                    //add to list new user.
                    FileStream fs3 = new FileStream(@"data\users.xml", FileMode.Create, FileAccess.Write);
                    userData new_user = new userData(ClientInformation[1], ClientInformation[2]);
                    userList.Add(new_user);
                    xm.Serialize(fs3, userList);
                    fs3.Close();

                    result = "The user has registered successfully";
                }
            }
            if (ClientInformation[0].Equals("Logout"))
            {
                int index = is_logged_in(ClientLoginUsers, ClientInformation[1]);
                if (index != -1)
                {
                    ClientLoginUsers.Remove(ClientLoginUsers[index]);
                    result = "logged out";
                }
                else result = "user doesn't exist";
            }
            if (ClientInformation[0].Equals("CreateRoom"))
            {

                if (is_exist_room(roomList, ClientInformation[1]))
                {
                    result = "this room name is already taken, please try a diffrent name.";
                }
                else
                {
                    bool Locked = false, AllowsViewers = false;

                    if (ClientInformation[4].Equals("True")) Locked = true;
                    if (ClientInformation[5].Equals("True")) AllowsViewers = true;

                    Room r = new Room(socket, ClientInformation[1], ClientInformation[2], ClientInformation[3], Locked, AllowsViewers);
                    roomList.Add(r);
                    result = "room created seccesfully!!!";
                }
            }
            if (ClientInformation[0].Equals("JoinRoom"))
            {
                int index = locate_room_by_creatorname(roomList, ClientInformation[1]);
                if (index == -1)
                {
                    result = "ERROR: room doesn't exist";
                }
                else
                {
                    roomList[index]._memberName = ClientInformation[2];
                    roomList[index]._memberSocket = socket;
                    roomList[index]._hasGameStarted = true;

                    Random rnd = new Random();
                    int num = rnd.Next(1000) % 2;
                    
                    Game g = new Game(roomList[index]._creatorSocket, socket, roomList[index]._roomName, roomList[index]._creatorName, roomList[index]._memberName, roomList[index]._allowsViewer);
                    gameList.Add(g);

                    
                    result = "joined room " + g._roomName;


                    if (num == 1)
                    {
                        result += ", you will be the whites";
                        g._memberColor = true;
                        g._creatorColor = false;
                    }
                    else
                    {
                        result += ", you will be the blacks";
                        g._memberColor = false;
                        g._creatorColor = true;
                    }
                }
            }
            if (ClientInformation[0].Equals("HasGameBegun"))
            {
                bool success = false;
                int roomIndex = locate_room_by_creatorname(roomList, ClientInformation[1]);

                if (roomList[roomIndex]._hasGameStarted)
                {
                    success = true;

                    result = "The game has begun!!!";

                    int gameIndex = FindGameInstance(ClientInformation[1]);
                    
                    if (gameList[gameIndex]._creatorColor) result += ", you will be the whites";
                    else result += ", you will be the blacks";

                    roomList.Remove(roomList[roomIndex]);
                    
                }

                
                if (!success)
                {
                    result = "Be patient..";
                }

            }
            if (ClientInformation[0].Equals("QuickJoinRoom"))
            {
                bool success = false;
                for (int i = 0; i < roomList.Count; ++i)
                {
                    if(roomList[i]._memberName == "")
                    {
                        success = true;

                        Game g = new Game(roomList[i]._creatorSocket, socket, roomList[i]._roomName, roomList[i]._creatorName, ClientInformation[1], roomList[i]._allowsViewer);
                        gameList.Add(g);

                        result = "joined room " + g._roomName;
                        
                        roomList[i]._memberSocket = socket;
                        roomList[i]._hasGameStarted = true;
                        roomList[i]._memberName = ClientInformation[1];

                        Random rnd = new Random();
                        int num = rnd.Next(1000) % 2;
                        
                        if (num == 1) 
                        {
                            result += ", you will be the whites";
                            g._memberColor = true;
                            g._creatorColor = false;
                        } 
                        else 
                        {
                            result += ", you will be the blacks";
                            g._memberColor = false;
                            g._creatorColor = true;
                        }

                        break;
                    }
                }
                if (!success)
                {
                    Room r = new Room(socket, "QuickJoinRoom", ClientInformation[1], "", false, true);
                    roomList.Add(r);
                    result = "room has been created for you";
                }
            }
            if (ClientInformation[0].Equals("CloseRoom"))
            {
                
                int index = locate_room_by_creatorname(roomList, ClientInformation[1]);

                if (index == -1)
                {
                    result = "room doesn't exsit";
                }
                else 
                {
                    roomList.Remove(roomList[index]);
                    result = "room closed seccesfully";
                }   
            }
            if (ClientInformation[0].Equals("THREADCHECK"))
            {
                if (roomList.Count == 0) result = "Room list is empty";
                for (int i = 0; i < roomList.Count; ++i)
                {
                    if (roomList[i]._memberName.Equals(ClientInformation[1]))
                    {
                        result = roomList[i]._creatorName +" ask to join";
                        //roomList[i]._memberName = "ROOMLOCKED";
                        break;
                    }
                }
                if (result == "") result = "not asked to join";
            }
            if (ClientInformation[0].Equals("invitationRefuse"))
            {
                if (roomList.Count == 0) result = "Room list is empty";
                for (int i = 0; i < roomList.Count; ++i)
                {
                    if (roomList[i]._creatorName.Equals(ClientInformation[1]))
                    {
                        result = "invitation closed";
                        roomList[i]._memberName = "";
                        break;
                    }
                }
            }
            if (ClientInformation[0].Equals("MOVESEND"))
            {
                int index = FindGameInstance(ClientInformation[3]);
                gameList[index]._source = ClientInformation[1];
                gameList[index]._dest = ClientInformation[2];
                gameList[index]._senderName = ClientInformation[3];
                if (ClientInformation.Length == 5) gameList[index]._gameOver = true;
                result = "the move sent";
            }
            if (ClientInformation[0].Equals("MOVECHECK"))
            {
                result = "no move was done";
                int pointer = FindGameInstance(ClientInformation[1]);
                if(!gameList[pointer]._senderName.Equals(ClientInformation[1]) && !gameList[pointer]._senderName.Equals(" "))
                {
                    string source = gameList[pointer]._source;
                    string dest = gameList[pointer]._dest;
                    result = "OpponentMoved;" + source + ";" + dest;
                    if (gameList[pointer]._gameOver) result += ";GameOver";
                    gameList[pointer]._senderName = " ";
                }
                

            }

                send(result, socket);
        }
        private static void EndRecieveInformation(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }
        private static bool is_exist(List<userData> userList, string user_name, string password, bool use)
        {
            int length = userList.Count();
            for (int i = 0; i < length; i++)
            {
                if (userList[i]._userName.Equals(user_name) && (use ? userList[i]._password.Equals(password) : true))
                {
                    return true;
                }
            }

            return false;
        }

        private static bool is_exist_room(List<Room> roomList, string roomName)
        {
            int length = roomList.Count();
            for (int i = 0; i < length; i++)
            {
                if (roomList[i]._roomName.Equals(roomName))
                {
                    return true;
                }
            }

            return false;
        }

        private static int locate_room_by_creatorname(List<Room> roomList, string creatorname)
        {
            int length = roomList.Count();
            for (int i = 0; i < length; i++)
            {
                if (roomList[i]._creatorName.Equals(creatorname))
                {
                    return i;
                }
            }

            return -1;
        }

        private static int is_logged_in(List<loginUser> logedInList, string loginName)
        {
            int length = logedInList.Count();
            for (int i = 0; i < length; i++)
            {
                if (logedInList[i]._userName.Equals(loginName))
                {
                    return i;
                }
            }

            return -1;
        }

        private static bool legalName(string name)
        {
            bool cond1, cond2, cond3;

            cond1 = ((int)name[0] >= 69 && (int)name[0] <= 90) || ((int)name[0] >= 97 && (int)name[0] <= 122);
            cond2 = !name.Contains("&") && !name.Contains("|") && !name.Contains("`") && !name.Contains("@") && !name.Contains("*") && !name.Contains("^") || !name.Contains("|") && !name.Contains("(") && !name.Contains(")");
            cond3 = !name.Contains("ROOMLOCKED");

            return cond1 && cond2 && cond3;
        }

        int FindGameInstance(string username)
        {
            for (int i = 0; i < gameList.Count; i++)
            {
                if (gameList[i]._creatorName.Equals(username) || gameList[i]._memberName.Equals(username))
                {
                    return i;
                }
            }
            return 0;
        }

        private void send(string msg, Socket socket)
        {
            byte[] data = Encoding.ASCII.GetBytes(msg);
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), socket);
            socket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, new AsyncCallback(RecieveInformation), socket);
        }

    }
}

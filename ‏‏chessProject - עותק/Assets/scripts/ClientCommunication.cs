﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class ClientCommunication : MonoBehaviour
{
    private static Socket _client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
    string registerName = "", registerPassword = "", loginName = "", loginPassword = "", roomName = "", playerName = "", tryingToPlay = "";
    public static string userName = "";
    bool isAllowsViewers, isLocked = false, activeWaitingRoom = false;
    bool loginScreenOn = false, registerScreenOn = false, RoomChooseOn = false, CreateRoomScreenOn = false, WaitingRoomScreenOn = false, AskToPlayScreenOn = false, GameScreenOn = false, WelcomeScreenOn = false;
    int frameCounter = 0;
    public static bool isWhite = false;


    //Screen GameObject
    [SerializeField] GameObject loginScreen, registerScreen, RoomChoose, CreateRoomScreen, WaitingRoomScreen, AskToPlayScreen, GameScreen, AskToPlayText, WelcomeScreen;

    //UI Text GameObject
    static TextMeshProUGUI AskToPlayText_text;

    //public GameObject registerPasswordText;
    //TextMeshProUGUI registerPasswordText_text;

    public void Start()
    {
        AskToPlayText_text = AskToPlayText.GetComponent<TextMeshProUGUI>();
        Debug.Log("start is working");
        //registerPasswordText_text = registerPasswordText.GetComponent<TextMeshProUGUI>();
        //registerPasswordText_text.text = "blabla";

        ConnectToServer();
    }

    public void ConnectToServer()
    {
        if (!_client.Connected)
        {
            try
            {
                _client.Connect(IPAddress.Loopback, 55555);
            }
            catch (SocketException)
            {
                Debug.Log("not connected");
            }
            if (_client.Connected)
            {
                Debug.Log("Connection established");
            }
        }
    }

    public void SendInfrmaionToServer(string userInfo)
    {
        if (_client.Connected)
        {
            byte[] userData = Encoding.ASCII.GetBytes(userInfo);
            _client.Send(userData);
        }
        else
        {
            Debug.Log("The connection is over");
        }
    }

    public string ReciveInformationFromServer()
    {
        try
        {
            byte[] reciveBuffer = new byte[1024];
            int rec = _client.Receive(reciveBuffer);
            byte[] data = new byte[rec];
            Array.Copy(reciveBuffer, data, rec);
            string returnFormServer = Encoding.ASCII.GetString(data);
            return returnFormServer;
        }
        catch (Exception)
        {
            Debug.Log("The connection is over");
        }
        return "";
    }

    public void register_Click()
    {
        string send = "REGISTER;" + registerName + ";" + registerPassword;
        userName = registerName;
        SendInfrmaionToServer(send);
        string recieve = ReciveInformationFromServer();
        if (recieve.Equals("The user has registered successfully"))
        {
            activateScreen("registerScreen", false);
            activateScreen("RoomChoose", true);
        }
        Debug.Log(recieve);
    }

    public void login_Click()
    {
        string send = "LOGIN;" + loginName + ";" + loginPassword;
        userName = loginName;
        SendInfrmaionToServer(send);
        string recieve = ReciveInformationFromServer();
        if (recieve.Equals("loged in seccesfully!!!"))
        {
            activateScreen("loginScreen", false);
            activateScreen("RoomChoose", true);
        }
        Debug.Log(recieve);
    }

    public void createRoom_Click()
    {
        string Locked = "False", AllowsViewers = "False";

        if (isLocked) Locked = "True";
        if (isAllowsViewers) AllowsViewers = "True";

        string send = "CreateRoom;" + roomName + ";" + userName + ";" + playerName + ";" + Locked + ";" + AllowsViewers;

        SendInfrmaionToServer(send);
        string recieve = ReciveInformationFromServer();

        if (recieve.Equals("room created seccesfully!!!"))
        {
            activateScreen("CreateRoomScreen", false);
            activateScreen("WaitingRoomScreen", true);

        }
        Debug.Log(recieve);
    }
    public void closeRoom_Click()
    {

        string send = "CloseRoom;" + userName;

        SendInfrmaionToServer(send);
        string recieve = ReciveInformationFromServer();

        activateScreen("WaitingRoomScreen", false);
        activateScreen("RoomChoose", true);

        Debug.Log(recieve);
    }
    public void refuse_Click()
    {
        string creatorName = AskToPlayText_text.text.Substring(0, tryingToPlay.IndexOf(" "));
        string send = "invitationRefuse;" + creatorName;

        SendInfrmaionToServer(send);
        string recieve = ReciveInformationFromServer();

        activateScreen("AskToPlayScreen", false);

        Debug.Log(recieve);
    }
    public void logout_Click()
    {

        string send = "Logout;" + userName;

        SendInfrmaionToServer(send);
        string recieve = ReciveInformationFromServer();

        activateScreen("AskToPlayScreen", false);
        activateScreen("WelcomeScreen", true);


        Debug.Log(recieve);
    }

    public void searchRoom_Click()
    {
        SendInfrmaionToServer("QuickJoinRoom;" + userName);
        string recieve = ReciveInformationFromServer();

        if (recieve.Contains("joined room"))
        {
            activateScreen("RoomChoose", false);
            activateScreen("GameScreen", true);

        }
        else if (recieve.Equals("room has been created for you"))
        {
            activateScreen("RoomChoose", false);
            activateScreen("WaitingRoomScreen", true);

        }
        Debug.Log(recieve);
    }

    ////////////////// UTILS FUNCTION ////////////////


    public void ReadRoomName(string name)
    {
        roomName = name;
    }
    public void ReadPlayerName(string name)
    {
        playerName = name;
    }

    public void updateLocker(bool locker)
    {
        isLocked = locker;
    }
    public void updateAllowsViewers(bool allowsViewers)
    {
        isAllowsViewers = allowsViewers;
    }

    public void ReadRegisterName(string userName)
    {
        registerName = userName;
    }
    public void ReadRegisterPassword(string password)
    {
        registerPassword = password;
    }
    public void ReadLoginPassword(string password)
    {
        loginPassword = password;
    }
    public void ReadLoginName(string userName)
    {
        loginName = userName;
    }
    public string GetUserName()
    {
        return userName;
    }
    public bool GetUserColor()
    {
        return isWhite;
    }

    void Update()
    {
        if (frameCounter != 60)
        {
            frameCounter++;
            return;
        }

        if (CreateRoomScreenOn || RoomChooseOn)
        {
            SendInfrmaionToServer("THREADCHECK;" + userName);
            string tryingToPlay = ReciveInformationFromServer();

            if (tryingToPlay.Contains("ask to join"))
            {
                activateScreen("AskToPlayScreen", true);
                AskToPlayText_text.text = tryingToPlay;
            }
        }

        if (WaitingRoomScreenOn)
        {
            SendInfrmaionToServer("HasGameBegun;" + userName);
            string gameStarted = ReciveInformationFromServer();
            if (gameStarted.Contains("The game has begun!!!"))
            {
                if (gameStarted.Contains("whites")) isWhite = true;
                activateScreen("WaitingRoomScreen", false);
                activateScreen("GameScreen", true);
                Debug.Log(gameStarted);
            }
        }


        frameCounter = 0;
    }

    public void joinRoom_Click()
    {
        string creatorName = AskToPlayText_text.text.Substring(0, tryingToPlay.IndexOf(" "));

        SendInfrmaionToServer("JoinRoom;" + creatorName + ";" + userName);
        string recieve = ReciveInformationFromServer();

        if (recieve.Contains("joined room"))
        {
            if (recieve.Contains("whites")) isWhite = true;
            activateScreen("GameScreen", true);
            activateScreen("CreateRoomScreen", false);
            activateScreen("RoomChoose", false);
            activateScreen("AskToPlayScreen", false);

        }
        if (recieve.Contains("ERROR: room doesn't exist"))
        {
            activateScreen("AskToPlayScreen", false);
        }
        Debug.Log(recieve);
    }

    public void activateScreen(string screenName, bool change)
    {
        switch (screenName)
        {
            case "loginScreen":
                loginScreen.SetActive(change);
                loginScreenOn = change;
                break;
            case "registerScreen":
                registerScreen.SetActive(change);
                registerScreenOn = change;
                break;
            case "RoomChoose":
                RoomChoose.SetActive(change);
                RoomChooseOn = change;
                break;
            case "CreateRoomScreen":
                CreateRoomScreen.SetActive(change);
                CreateRoomScreenOn = change;
                break;
            case "WaitingRoomScreen":
                WaitingRoomScreen.SetActive(change);
                WaitingRoomScreenOn = change;
                break;
            case "AskToPlayScreen":
                AskToPlayScreen.SetActive(change);
                AskToPlayScreenOn = change;
                break;
            case "GameScreen":
                SceneManager.LoadScene("Chess Set");
                break;
            case "WelcomeScreen":
                WelcomeScreen.SetActive(change);
                WelcomeScreenOn = change;
                break;
            default:
                Debug.Log("Error: screen name not define");
                break;

        }
    }
}

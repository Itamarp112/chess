using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PawnMovement : MonoBehaviour
{
    Vector3 lastTurn;
    [SerializeField] GameObject me;
    public GameObject test;
    public GameObject dest;
    int IsPressed = 3;
    string myName;
    bool moves = false;
    // Start is called before the first frame update
    void Start()
    {
        test = GameObject.Find("E4");
        myName = me.name;
    }
    
    void OnMouseDown()
    {
        FindObjectOfType<ChessMovementManager>().Source(myName);
        IsPressed = 2;


    }

    public void Move(string Dest)
    {
        dest = GameObject.Find(Dest);
        moves = true;
        //transform.position = Vector3.MoveTowards(transform.position, dest.transform.position, Time.deltaTime * 1);
    }


    void Update()
    {
        if (Input.GetMouseButtonDown(0) && IsPressed == 1)
        {
            IsPressed = 3;
        }
        else if (IsPressed == 2)
        {
            IsPressed--;
        }
        //transform.position = Vector3.MoveTowards(transform.position, test.transform.position, Time.deltaTime * 1);
        if (moves)
        {
            transform.position = Vector3.MoveTowards(transform.position, dest.transform.position, Time.deltaTime * 1);
            
            if (transform.position.Equals(lastTurn))
            {
                moves = false;
                FindObjectOfType<ChessMovementManager>().Source("");
            }
            lastTurn = transform.position;

        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ChessMovementManager : MonoBehaviour
{

    //All game object.
    public PawnMovement movement;
    public ClientCommunication communicationScript;
    public GameObject src;
    public GameObject camObject;
    public GameObject camPosition;
    public MeshCollider[] blackColliders;
    public MeshCollider[] whiteColliders;
    public GameObject blackPieces;
    public GameObject whitePieces;

    //Global variables.
    int count = 0;
    string myToolName = "";
    string myToolDest = "";
    string clientName = "";
    static bool turn;
    public static bool isWhite;
    Board board = new Board();
    public static bool gameOver = false;

    void Start()
    {
        //Get game information.
        turn = communicationScript.GetUserColor();
        isWhite = communicationScript.GetUserColor();
        clientName = communicationScript.GetUserName();

        //Update black settings.
        if (!isWhite)
        {
            SetAsBlack();
        }
    }

    //Every time tool pressed.
    public void Source(string name)
    {
        myToolName = name;
    }

    //Evrey time square pressed.
    public void Dest(string squareName)
    {
        myToolDest = squareName;

        if (turn && board.CheckLegalMove(myToolName, myToolDest) && !gameOver)
        {
            string updateMessage = "";
            //Update server.
            if (gameOver)
            {
                Debug.Log("GameOver!!!");
                updateMessage = "MOVESEND;" + myToolName + ";" + myToolDest + ";" + clientName + ";GameOver";
            }
            else
            {
                updateMessage = "MOVESEND;" + myToolName + ";" + myToolDest + ";" + clientName;
            }

            communicationScript.SendInfrmaionToServer(updateMessage);
            string response = communicationScript.ReciveInformationFromServer();

            //Update game.
            src = GameObject.Find(myToolName);
            movement = src.GetComponent<PawnMovement>();
            movement.Move(myToolDest);

            turn = false;
        }
    }

    void Update()
    {
        if (!turn && !gameOver)
        {
            if (count == 60)
            {
                //Get turn Update from server.
                communicationScript.SendInfrmaionToServer("MOVECHECK;" + clientName);
                string response = communicationScript.ReciveInformationFromServer();
                string[] responseSplit = response.Split(';');

                if (responseSplit.Length == 4 && responseSplit.Equals("GameOver"))
                {
                    gameOver = true;
                    Debug.Log("GameOver!!!");
                }

                if (responseSplit[0].Equals("OpponentMoved"))
                {
                    string enemyToolName = responseSplit[1];
                    string enemyToolDest = responseSplit[2];

                    //Update board.
                    board.updateEnemyMove(enemyToolName, enemyToolDest);
                    src = GameObject.Find(enemyToolName);

                    //Update game.
                    movement = src.GetComponent<PawnMovement>();
                    movement.Move(enemyToolDest);
                    turn = true;
                }
                count = 0;
            }
            else count++;
        }
    }

    void SetAsBlack()
    {
        //Camera settings.
        camObject.transform.position = camPosition.transform.position;
        camObject.transform.rotation = camPosition.transform.rotation;

        //Turn on black tools Colliders.
        blackColliders = blackPieces.GetComponentsInChildren<MeshCollider>();
        foreach (MeshCollider mesh in blackColliders)
        {
            mesh.enabled = true;
        }

        //Turn OF white tools Colliders.
        whiteColliders = whitePieces.GetComponentsInChildren<MeshCollider>();
        foreach (MeshCollider mesh in whiteColliders)
        {
            mesh.enabled = false;
        }
    }
}

public class Board
{
    public Board()
    {
        //Init chess bord and dict.
        DictionarySet();
        ChessBoardStart();
    }

    //Dict from location to indexes.
    public IDictionary<string, (int, int)> dict;

    //2D array board that save the game.
    public static ChessTool[,] _ChessBoard { set; get; }

    public void ChessBoardStart()
    {
        _ChessBoard = new ChessTool[8, 8];

        //White tools.
        Rock RockWhite = new Rock(true, "Rock White", 7, 7);
        Rock RockWhite2 = new Rock(true, "Rock White 2", 7, 0);
        Knight KnightWhite = new Knight(true, "Knight White", 7, 6);
        Knight KnightWhite2 = new Knight(true, "Knight White 2", 7, 1);
        Bishop BishopWhite = new Bishop(true, "Bishop White", 7, 5);
        Bishop BishopWhite2 = new Bishop(true, "Bishop White 2", 7, 2);
        King KingWhite = new King(true, "King White", 7, 4);
        Queen QueenWhite = new Queen(true, "Queen White", 7, 3);

        _ChessBoard[7, 0] = RockWhite2;
        _ChessBoard[7, 1] = KnightWhite2;
        _ChessBoard[7, 2] = BishopWhite2;
        _ChessBoard[7, 3] = QueenWhite;
        _ChessBoard[7, 4] = KingWhite;
        _ChessBoard[7, 5] = BishopWhite;
        _ChessBoard[7, 6] = KnightWhite;
        _ChessBoard[7, 7] = RockWhite;

        //White pawns.
        Pawn PawnWhite = new Pawn(true, "Pawn White", 6, 0);
        Pawn PawnWhite2 = new Pawn(true, "Pawn White 2", 6, 1);
        Pawn PawnWhite3 = new Pawn(true, "Pawn White 3", 6, 2);
        Pawn PawnWhite4 = new Pawn(true, "Pawn White 4", 6, 3);
        Pawn PawnWhite5 = new Pawn(true, "Pawn White 5", 6, 4);
        Pawn PawnWhite6 = new Pawn(true, "Pawn White 6", 6, 5);
        Pawn PawnWhite7 = new Pawn(true, "Pawn White 7", 6, 6);
        Pawn PawnWhite8 = new Pawn(true, "Pawn White 8", 6, 7);

        _ChessBoard[6, 0] = PawnWhite;
        _ChessBoard[6, 1] = PawnWhite2;
        _ChessBoard[6, 2] = PawnWhite3;
        _ChessBoard[6, 3] = PawnWhite4;
        _ChessBoard[6, 4] = PawnWhite5;
        _ChessBoard[6, 5] = PawnWhite6;
        _ChessBoard[6, 6] = PawnWhite7;
        _ChessBoard[6, 7] = PawnWhite8;

        //Black tools.
        Rock RockBlack = new Rock(false, "Rock Black", 0, 0);
        Rock RockBlack2 = new Rock(false, "Rock Black 2", 0, 7);
        Knight KnightBlack = new Knight(false, "Knight Black", 0, 1);
        Knight KnightBlack2 = new Knight(false, "Knight Black 2", 0, 6);
        Bishop BishopBlack = new Bishop(false, "Bishop Black", 0, 2);
        Bishop BishopBlack2 = new Bishop(false, "Bishop Black 2", 0, 5);
        Queen QueenBlack = new Queen(false, "Queen Black", 0, 3);
        King KingBlack = new King(false, "King Black", 0, 4);

        _ChessBoard[0, 0] = RockBlack;
        _ChessBoard[0, 1] = KnightBlack;
        _ChessBoard[0, 2] = BishopBlack;
        _ChessBoard[0, 3] = QueenBlack;
        _ChessBoard[0, 4] = KingBlack;
        _ChessBoard[0, 5] = BishopBlack2;
        _ChessBoard[0, 6] = KnightBlack2;
        _ChessBoard[0, 7] = RockBlack2;

        //Black pawns.
        Pawn PawnBlack = new Pawn(false, "Pawn Black", 1, 7);
        Pawn PawnBlack2 = new Pawn(false, "Pawn Black 2", 1, 6);
        Pawn PawnBlack3 = new Pawn(false, "Pawn Black 3", 1, 5);
        Pawn PawnBlack4 = new Pawn(false, "Pawn Black 4", 1, 4);
        Pawn PawnBlack5 = new Pawn(false, "Pawn Black 5", 1, 3);
        Pawn PawnBlack6 = new Pawn(false, "Pawn Black 6", 1, 2);
        Pawn PawnBlack7 = new Pawn(false, "Pawn Black 7", 1, 1);
        Pawn PawnBlack8 = new Pawn(false, "Pawn Black 8", 1, 0);

        _ChessBoard[1, 7] = PawnBlack;
        _ChessBoard[1, 6] = PawnBlack2;
        _ChessBoard[1, 5] = PawnBlack3;
        _ChessBoard[1, 4] = PawnBlack4;
        _ChessBoard[1, 3] = PawnBlack5;
        _ChessBoard[1, 2] = PawnBlack6;
        _ChessBoard[1, 1] = PawnBlack7;
        _ChessBoard[1, 0] = PawnBlack8;
    }

    public void DictionarySet()
    {
        dict = new Dictionary<string, (int, int)>();

        dict.Add(("A1"), (7, 0));
        dict.Add(("A2"), (6, 0));
        dict.Add(("A3"), (5, 0));
        dict.Add(("A4"), (4, 0));
        dict.Add(("A5"), (3, 0));
        dict.Add(("A6"), (2, 0));
        dict.Add(("A7"), (1, 0));
        dict.Add(("A8"), (0, 0));

        dict.Add(("B1"), (7, 1));
        dict.Add(("B2"), (6, 1));
        dict.Add(("B3"), (5, 1));
        dict.Add(("B4"), (4, 1));
        dict.Add(("B5"), (3, 1));
        dict.Add(("B6"), (2, 1));
        dict.Add(("B7"), (1, 1));
        dict.Add(("B8"), (0, 1));

        dict.Add(("C1"), (7, 2));
        dict.Add(("C2"), (6, 2));
        dict.Add(("C3"), (5, 2));
        dict.Add(("C4"), (4, 2));
        dict.Add(("C5"), (3, 2));
        dict.Add(("C6"), (2, 2));
        dict.Add(("C7"), (1, 2));
        dict.Add(("C8"), (0, 2));

        dict.Add(("D1"), (7, 3));
        dict.Add(("D2"), (6, 3));
        dict.Add(("D3"), (5, 3));
        dict.Add(("D4"), (4, 3));
        dict.Add(("D5"), (3, 3));
        dict.Add(("D6"), (2, 3));
        dict.Add(("D7"), (1, 3));
        dict.Add(("D8"), (0, 3));

        dict.Add(("E1"), (7, 4));
        dict.Add(("E2"), (6, 4));
        dict.Add(("E3"), (5, 4));
        dict.Add(("E4"), (4, 4));
        dict.Add(("E5"), (3, 4));
        dict.Add(("E6"), (2, 4));
        dict.Add(("E7"), (1, 4));
        dict.Add(("E8"), (0, 4));

        dict.Add(("F1"), (7, 5));
        dict.Add(("F2"), (6, 5));
        dict.Add(("F3"), (5, 5));
        dict.Add(("F4"), (4, 5));
        dict.Add(("F5"), (3, 5));
        dict.Add(("F6"), (2, 5));
        dict.Add(("F7"), (1, 5));
        dict.Add(("F8"), (0, 5));

        dict.Add(("G1"), (7, 6));
        dict.Add(("G2"), (6, 6));
        dict.Add(("G3"), (5, 6));
        dict.Add(("G4"), (4, 6));
        dict.Add(("G5"), (3, 6));
        dict.Add(("G6"), (2, 6));
        dict.Add(("G7"), (1, 6));
        dict.Add(("G8"), (0, 6));

        dict.Add(("H1"), (7, 7));
        dict.Add(("H2"), (6, 7));
        dict.Add(("H3"), (5, 7));
        dict.Add(("H4"), (4, 7));
        dict.Add(("H5"), (3, 7));
        dict.Add(("H6"), (2, 7));
        dict.Add(("H7"), (1, 7));
        dict.Add(("H8"), (0, 7));
    }

    bool makeCheckMate()
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (_ChessBoard[i, j] != null && _ChessBoard[i, j]._isWhite != ChessMovementManager.isWhite)
                {
                    if (tryAnyMove(_ChessBoard[i, j]))
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    bool tryAnyMove(ChessTool tool)
    {
        ChessTool temp = null;
        int row = tool._locationX;
        int col = tool._locationY;

        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (tool.IsLegalMove(i, j) && !(i == row && j == col) && (_ChessBoard[i, j] == null || (_ChessBoard[i, j] != null && _ChessBoard[i, j]._isWhite != _ChessBoard[row, col]._isWhite)))
                {
                    //replayTurn(temp, row, col, i, j);
                    playTurn(row, col, i, j);

                    if (!makeCheck(tool._isWhite))
                    {
                        replayTurn(temp, row, col, i, j);
                        return true;
                    }
                    replayTurn(temp, row, col, i, j);
                }
            }
        }

        return false;
    }

    void replayTurn(ChessTool save, int src_row, int src_col, int dst_row, int dst_col)
    {
        _ChessBoard[src_row, src_col] = _ChessBoard[dst_row, dst_col];
        
        _ChessBoard[src_row, src_col]._locationX = src_row;
        _ChessBoard[src_row, src_col]._locationY = src_col;



        _ChessBoard[dst_row, dst_col] = save;
        _ChessBoard[dst_row, dst_col]._locationX = dst_row;
        _ChessBoard[dst_row, dst_col]._locationY = dst_col;
    }

    public Tuple<int, int> findToolLocation(string toolName)
    {
        for (int i = 0; i < 8; i++)
        {
            for (int j = 0; j < 8; j++)
            {
                if (_ChessBoard[i, j] != null && _ChessBoard[i, j]._name.Equals(toolName))
                {
                    Debug.Log("Row: " + i + ", Col: " + j);
                    return Tuple.Create(i, j);
                }
            }
        }
        return Tuple.Create(0, 0);
    }

    public bool CheckLegalMove(string source, string dest)
    {
        ChessTool temp;
        var dstLocation = dict[dest];
        var srcLocation = findToolLocation(source);

        int src_col = srcLocation.Item2;
        int src_row = srcLocation.Item1;

        int dst_col = dstLocation.Item2;
        int dst_row = dstLocation.Item1;


        if (src_row == dst_row && src_col == dst_col)
        {
            Debug.Log("Same Square");
            return false;
        }
        else if (_ChessBoard[dst_row, dst_col] != null && _ChessBoard[dst_row, dst_col]._isWhite == _ChessBoard[src_row, src_col]._isWhite)
        {
            Debug.Log("Same color or player dont exist");
            return false;
        }
        else if (!_ChessBoard[src_row, src_col].IsLegalMove(dst_row, dst_col))
        {
            Debug.Log("Not valid move");
            return false;
        }
        else
        {
            //made the move.
            temp = playTurn(src_row, src_col, dst_row, dst_col);



            //Check if you made chess on yourself.
            //if (makeCheck(ChessMovementManager.isWhite))
            //{
            //    replayTurn(temp, src_row, src_col, dst_row, dst_col);
            //    Debug.Log("Made chess on yourself");
            //    return false;
            //}

            //ChessMovementManager.gameOver = makeCheckMate();
            //Debug.Log("Made chess on enemy");
            return true;
        }

    }

    public void updateEnemyMove(string source, string dest)
    {
        var srcLocation = dict[dest];
        var dstLocation = findToolLocation(source);

        int y = srcLocation.Item2;
        int x = srcLocation.Item1;

        int j = dstLocation.Item2;
        int i = dstLocation.Item1;

        Debug.Log("srcX: " + i + ", srcY: " + j);
        Debug.Log("dstX: " + x + ", dstY: " + y);

        _ChessBoard[x, y] = _ChessBoard[i, j];
        _ChessBoard[x, y]._locationX = x;
        _ChessBoard[x, y]._locationY = y;
        _ChessBoard[i, j] = null;
    }

    public bool makeCheck(bool white)
    {
        bool searchAfterKing = true;
        bool searchAfterCheck = true;
        int src_row = 0, src_col = 0, dst_row = 0, dst_col = 0;

        for (int i = 0; i < 8 && searchAfterKing; i++)
        {
            for (int j = 0; j < 8 && searchAfterKing; j++)
            {
                if (_ChessBoard[i, j] != null && _ChessBoard[i, j]._isWhite == white && _ChessBoard[i, j]._name.Contains("King"))
                {
                    dst_row = i;
                    dst_col = j;
                    searchAfterKing = false;
                }
            }
        }

        for (int i = 0; i < 8 && searchAfterCheck; i++)
        {
            for (int j = 0; j < 8 && searchAfterCheck; j++)
            {
                if (_ChessBoard[i, j] != null && _ChessBoard[i, j]._isWhite != white && _ChessBoard[i, j].IsLegalMove(dst_row, dst_col))
                {
                    return true;
                }
            }
        }

        return false;
    }

    public ChessTool playTurn(int src_row, int src_col, int dst_row, int dst_col)
    {
        ChessTool temp = _ChessBoard[dst_row, dst_col];
        _ChessBoard[dst_row, dst_col] = _ChessBoard[src_row, src_col];
        _ChessBoard[dst_row, dst_col]._locationX = dst_row;
        _ChessBoard[dst_row, dst_col]._locationY = dst_col;

        _ChessBoard[src_row, src_col] = null;
        return temp;
    }
}

public class ChessTool
{
    public bool _isWhite { set; get; }
    public string _name { set; get; }
    public int _locationX { set; get; }
    public int _locationY { set; get; }

    public ChessTool(bool color, string name, int locationX, int locationY)
    {
        _isWhite = color;
        _name = name;
        _locationY = locationY;
        _locationX = locationX;

    }
    public virtual bool IsLegalMove(int x, int y)
    {
        return false;
    }
}

public class Rock : ChessTool
{
    public Rock(bool _color, string _name, int _locationX, int _locationY) : base(_color, _name, _locationX, _locationY) { }

    public override bool IsLegalMove(int x, int y)
    {
        int dst_col, src_col, src_row, dst_row, temp;

        src_col = _locationY;
        dst_col = y;

        src_row = _locationX;
        dst_row = x;

        if (dst_col < src_col)
        {
            temp = dst_col;
            dst_col = src_col;
            src_col = temp;
        }
        if (dst_row < src_row)
        {
            temp = dst_row;
            dst_row = src_row;
            src_row = temp;
        }

        if (src_row == dst_row)
        {
            for (int i = src_col + 1; i < dst_col; i++)
            {
                if (Board._ChessBoard[src_row, i] != null)
                {
                    Debug.Log(Board._ChessBoard[src_row, i]._name);
                    Debug.Log("X: " + src_row + ", Y: " + i);
                    return false;
                }
            }
        }
        else if (src_col == dst_col)
        {
            for (int i = src_row + 1; i < dst_row; i++)
            {
                if (Board._ChessBoard[i, src_col] != null)
                {
                    Debug.Log(Board._ChessBoard[i, src_col]._name);
                    Debug.Log("X: " + i + ", Y: " + src_col);
                    return false;
                }
            }
        }
        else return false;
        return true;
    }
}

public class Pawn : ChessTool
{
    public Pawn(bool _color, string _name, int _locationX, int _locationY) : base(_color, _name, _locationX, _locationY) { }

    public override bool IsLegalMove(int x, int y)
    {
        bool canMove = false;
        int dst_col, src_col, src_row, dst_row;

        src_row = _locationX;
        src_col = _locationY;
        dst_row = x;
        dst_col = y;

        if (Board._ChessBoard[dst_row, dst_col] != null && Math.Abs(src_col - dst_col) == 1 && Math.Abs(src_row - dst_row) == 1)
        {
            if (_isWhite && src_row - dst_row == 1)
                canMove = true;
            if (!_isWhite && src_row - dst_row == -1)
                canMove = true;
        }
        else if (Board._ChessBoard[dst_row, dst_col] == null && src_col == dst_col && Math.Abs(src_row - dst_row) == 2)
        {
            if (_isWhite && src_row == 6 && Board._ChessBoard[2, dst_col] == null)
                canMove = true;
            if (!_isWhite && src_row == 1 && Board._ChessBoard[5, dst_col] == null)
                canMove = true;
        }
        else if (Board._ChessBoard[dst_row, dst_col] == null && src_col == dst_col && Math.Abs(src_row - dst_row) == 1)
        {
            if (_isWhite && src_row - dst_row == 1)
                canMove = true;
            if (!_isWhite && src_row - dst_row == -1)
                canMove = true;
        }

        //check if its queen.
        //if (canMove)
        //{
        //    if ((_isWhite && (_Board->getOrder()) ? dst_row == 0 : dst_row == 7) || (!_isWhite && (_Board->getOrder()) ? dst_row == 7 : dst_row == 0))
        //        _Board->getBoard()[src_row][src_col] = new Queen((_isWhite) ? 'Q' : 'q', _Board);
        //}
        return canMove;
    }
}

public class Knight : ChessTool
{
    public Knight(bool _color, string _name, int _locationX, int _locationY) : base(_color, _name, _locationX, _locationY) { }

    public override bool IsLegalMove(int x, int y)
    {
        bool canMove = false;
        int dst_col, src_col, src_row, dst_row;

        src_row = _locationX;
        src_col = _locationY;
        dst_row = x;
        dst_col = y;

        if (Math.Abs(dst_row - src_row) == 2 && Math.Abs(dst_col - src_col) == 1)
            return true;
        else if (Math.Abs(dst_row - src_row) == 1 && Math.Abs(dst_col - src_col) == 2)
            return true;
        else return false;
    }
}

public class Bishop : ChessTool
{
    public Bishop(bool _color, string _name, int _locationX, int _locationY) : base(_color, _name, _locationX, _locationY) { }

    public override bool IsLegalMove(int x, int y)
    {
        bool canMove = false;
        int dst_col, src_col, src_row, dst_row;

        src_row = _locationX;
        src_col = _locationY;
        dst_row = x;
        dst_col = y;

        if (Math.Abs(dst_col - src_col) == Math.Abs(dst_row - src_row))
        {
            while (Math.Abs(dst_col - src_col) > 1)
            {
                if (src_col < dst_col) src_col++;
                else src_col--;

                if (src_row < dst_row) src_row++;
                else src_row--;

                if (Board._ChessBoard[src_row, src_col] != null)
                    return false;
            }
        }
        else return false;

        return true;
    }
}

public class Queen : ChessTool
{
    public Queen(bool _color, string _name, int _locationX, int _locationY) : base(_color, _name, _locationX, _locationY) { }

    public override bool IsLegalMove(int x, int y)
    {
        bool canMove = false;
        int dst_col, src_col, src_row, dst_row;

        src_row = _locationX;
        src_col = _locationY;
        dst_row = x;
        dst_col = y;

        Rock r = new Rock(_isWhite, _name, _locationX, _locationY);
        Bishop b = new Bishop(_isWhite, _name, _locationX, _locationY);

        if (r.IsLegalMove(x, y) || b.IsLegalMove(x, y)) return true;

        return false;
    }
}

public class King : ChessTool
{
    public King(bool _color, string _name, int _locationX, int _locationY) : base(_color, _name, _locationX, _locationY) { }

    public override bool IsLegalMove(int x, int y)
    {
        bool canMove = false;
        int dst_col, src_col, src_row, dst_row;

        src_row = _locationX;
        src_col = _locationY;
        dst_row = x;
        dst_col = y;

        if (Math.Abs(dst_row - src_row) < 2 && Math.Abs(dst_col - src_col) < 2)
            return true;

        return false;
    }
}
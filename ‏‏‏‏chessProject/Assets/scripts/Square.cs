using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square : MonoBehaviour

{
    [SerializeField] GameObject me;
    string myName;
    void Start()
    {
        myName = me.name;

    }

    void OnMouseDown()
    {
        Debug.Log("square pressed");
        FindObjectOfType<ChessMovementManager>().Dest(myName);


    }
}
